/**
 * ReplaceAll by Fagner Brack (MIT Licensed)
 * Replaces all occurrences of a substring in a string
 */
String.prototype.replaceAll = function( token, newToken, ignoreCase ) {
    var _token;
    var str = this + "";
    var i = -1;

    if ( typeof token === "string" ) {

        if ( ignoreCase ) {

            _token = token.toLowerCase();

            while( (
                i = str.toLowerCase().indexOf(
                    token, i >= 0 ? i + newToken.length : 0
                ) ) !== -1
            ) {
                str = str.substring( 0, i ) +
                    newToken +
                    str.substring( i + token.length );
            }

        } else {
            return this.split( token ).join( newToken );
        }

    }
return str;
};

$(function() {
    // find variables
    var variables = $('#variables')
    // find template
    var template = $('#template', variables)
    // remove function
    var remove = function(event) {
        console.log('fuu;')
        $(this).parents('.row:first').remove()
    }
    
    $('#add', variables).click(function(event) {
        // create new item
        var item = template.clone()
        // remove id
        item.attr('id', null)
        // assign remove action
        $('button.remove', item).click(remove)
        // add
        template.before(item)
    })
    
    $('#do').click(function(event) {
        var text = $('#in').val()
        $('#variables .row').each(function(num, obj) {
            if ($(this).attr('id') == null) {
                var from = $('input.name', this).val()
                var to = $('input.value', this).val()
                
                text = text.replaceAll(from.toLowerCase(), to, true)
            }
        })
        $('#out').val(text)
    })
});